# nodejs blockchain barebones settlement client #

## Getting Started ##
A sample use-case of a application that can make Settlement requests. To get started you should take the following steps

 * Clone this repository
 * Edit the application.proto file and assign your APP_ID for value of package
 * Goto Mastercard Developers and create a Mastercard Blockchain project (note this is currently a private API and you may need to request access). You will be taken through the wizard to create a node. You must provide an APP_ID and a protocol buffer definition i.e. application.proto.
 * You will receive a p12 file and a consumer key from Mastercard Developers for your project.
 * Execute the following commands
```bash
npm install
node app.js
```
When started it gets you to confirm your parameters and then displays a simple menu. 

## Menu ##
```
============ MENU ============
1. Create node (optional, onetime)
2. Update protocol buffer definition
3. Create settlement request
4. Confirm settlement
5. Show Protocol Buffer Definition
6. Re-initialize API
7. Print Command Line Options
0. Quit
Option:  (0)
```

## More Commandline Options ##
```
Options:
  --help                Show help                                      [boolean]
  --version             Show version number                            [boolean]
  --consumerKey, --ck   consumer key (mastercard developers)
  --keystorePath, --kp  the path to your keystore (mastercard developers)
  --keyAlias, --ka      key alias (mastercard developers)
  --storePass, --sp     keystore password (mastercard developers)
  --protoFile, --pf     protobuf file
  --verbosity, -v       log mastercard developers sdk to console[default: false]
```

### Useful Info
This project makes use of the Mastercard Blockchain SDK available from npm.

```bash
npm install mastercard-blockchain --save
```
