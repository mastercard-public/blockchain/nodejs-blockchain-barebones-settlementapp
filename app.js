#!/usr/bin/env node
var blockchain = require('mastercard-blockchain');
var MasterCardAPI = blockchain.MasterCardAPI;

const async = require('async'), encoding = 'hex', fs = require('fs');
var prompt = require('prompt'), options = createOptions();

var protobuf = require("protobufjs");

var argv = options.argv;
prompt.override = argv;

var protoFile = null;
var mst1ID = 1297306673;
var appID = null;
var msgClassDef = null;

console.log(fs.readFileSync('help.txt').toString());
prompt.start();
initApi((err, result) => {
  if (!err) {
   updateNode((error, data) => {
      processCommands();
   });
  }
});

function processCommands() {
  async.waterfall([
    function (callback) {
      prompt.get(promptCmdSchema(), callback);
    }
  ], function (err, result) {
    if (!err) {
      switch (result.cmdOption) {
        case 1:
          createNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 2:
          updateNode((error, data) => {
            processCommandsAfter();
          });
          break;
        case 3:
          createSettlementRequest((error, data) => {
            processCommandsAfter();
          });
          break;
        case 4:
          confirmSettlement((error, data) => {
            processCommandsAfter();
          });
          break;
        case 5:
          fs.readFile(protoFile, (err, data) => {
            if (err) {
              console.log('error', err);
            } else {
              console.log(data.toString());
            }
            processCommandsAfter();
          });
          break;
        case 6:
          done = true;
          initApi((err, result) => {
            if (!err) {
              processCommandsAfter();
            }
          });
          break;
        case 7:
          options.showHelp();
          async.nextTick(processCommandsAfter);
          break;
        default:
          console.log('Goodbye');
          break;
      }
    }
  });
}

function createNode(callback) {
  console.log('createNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'application.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        var appID = nested[0];
        var msgClassDef = root.lookupType(appID + "." + nested[1]);
        blockchain.Node.provision({
          network: 'ZONE',
          application: {
            name: appID,
            description: "",
            version: 0,
            definition: {
              format: "proto3",
              encoding: 'base64',
              messages: fs.readFileSync(protoFile).toString('base64')
            }
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function updateNode(callback) {
  console.log('updateNode');
  async.waterfall([
    function (callback) {
      prompt.get({
        name: 'newProtoFile',
        description: 'the path to the protobuf File',
        default: 'application.proto',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      }, callback);
    },
    function (data, callback) {
      protoFile = data.newProtoFile;
      protobuf.load(protoFile, callback);
    },
    function (root, callback) {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        var appID = nested[0];
        var msgClassDef = root.lookupType(appID + "." + nested[1]);
        blockchain.App.update({
          id: appID,
          name: appID,
          description: "",
          version: 0,
          definition: {
            format: "proto3",
            encoding: 'base64',
            messages: fs.readFileSync(protoFile).toString('base64')
          }
        }, callback);
      } else {
        callback('could not read message class def from proto file', null);
      }
    },
    function (result, callback) {
      blockchain.App.read(appID, {}, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}


function createSettlementRequest(callback) {
  console.log('createSettlementRequest');
  async.waterfall([
    function (callback) {
      prompt.get(promptSettleEntrySchema(), callback);
    },
    function (data, callback) {
      var payload = { from: data.from, to: data.to, amountMinorUnits: parseInt(data.amount), currency: data.currency, description: data.description, nonce: new Date().getTime() };
      var errMsg = msgClassDef.verify(payload);
      if (errMsg) {
        callback(errMsg, null);
      } else {
        var message = msgClassDef.create(payload);
        blockchain.TransactionEntry.create({
          "app": mst1ID,
          "encoding": encoding,
          "value": msgClassDef.encode(message).finish().toString(encoding)
        }, callback);
      }
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log(result);
    }
    async.nextTick(callback, err, result);
  });
}

function confirmSettlement(callback) {
  console.log('confirmSettlement');
  var ctx = {};
  async.waterfall([
    function (callback) {
      prompt.get(['hash'], callback);
    },
    function (data, callback) {
      blockchain.Settle.create({ "encoding": encoding, "hash": data.hash }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    } else {
      console.log('response', result);
    }
    async.nextTick(callback, err, ctx.data, result);
  });
}

function initApi(onDone) {
  console.log('initializing');
  async.waterfall([
    function (callback) {
      prompt.get(promptInitSchema(), callback);
    },
    function (result, callback) {
      var authentication = new MasterCardAPI.OAuth(result.consumerKey, result.keystorePath, result.keyAlias, result.storePass);
      MasterCardAPI.init({
        sandbox: true,
        debug: argv.verbosity,
        authentication: authentication
      });
      protoFile = result.protoFile;
      protobuf.load('settle.proto', callback);
    }
  ], function (err, root) {
    if (err) {
      console.log('error', err);
    } else {
      var nested = guessNested(root);
      if (nested && 2 == nested.length) {
        appID = nested[0];
        msgClassDef = root.lookupType(appID + "." + nested[1]);
        console.log('initialized');
      } else {
        console.log('could not read message class def from', protoFile);
      }
    }
    async.nextTick(onDone, err, appID);
  });
}

function processCommandsAfter() {
  async.waterfall([
    function (callback) {
      prompt.get({ properties: { enter: { description: 'press enter to continue', required: false } } }, callback);
    }
  ], function (err, result) {
    if (err) {
      console.log('error', err);
    }
    async.nextTick(processCommands);
  });
}

function promptInitSchema() {
  return {
    properties: {
      keystorePath: {
        description: 'the path to your keystore (mastercard developers)',
        required: true,
        conform: (value) => {
          return fs.existsSync(value);
        }
      },
      storePass: {
        description: 'keystore password (mastercard developers)',
        required: true,
        default: 'keystorepassword'
      },
      consumerKey: {
        description: 'consumer key (mastercard developers)',
        required: true
      },
      keyAlias: {
        description: 'key alias (mastercard developers)',
        required: true,
        default: 'keyalias'
      },
      protoFile: {
        description: 'the path to the protobuf File',
        required: true,
        default: 'application.proto'
      }
    }
  };
}

function promptSettleEntrySchema() {
  return {
    properties: {
      from: {
        description: 'from',
        required: true,
        default: 'GB111111111111'
      },
      to: {
        description: 'to',
        required: true,
        default: 'IE222222222222'
      },
      amount: {
        description: 'amount',
        required: true,
        default: '1234',
        conform: (value) => {
          return parseInt(value) > 0;
        }
      },
      currency: {
        description: 'currency',
        required: true,
        default: 'USD'
      },
      description: {
        description: 'description',
        required: true,
        default: 'This is a test.'
      },
    }
  };
}

function promptCmdSchema() {
  return {
    properties: {
      cmdOption: {
        description: '============ MENU ============\n1. Create node (optional, onetime)\n2. Update protocol buffer definition\n3. Create settlement request\n4. Confirm settlement\n5. Show Protocol Buffer Definition\n6. Re-initialize API\n7. Print Command Line Options\n0. Quit\nOption',
        message: 'Invalid Option',
        type: 'number',
        default: 0,
        required: true,
        conform: (value) => {
          return value >= 0 && value <= 7;
        }
      }
    }
  };
}

function createOptions() {
  return require('yargs')
    .options({
      'consumerKey': {
        alias: 'ck',
        description: 'consumer key (mastercard developers)'
      },
      'keystorePath': {
        alias: 'kp',
        description: 'the path to your keystore (mastercard developers)'
      },
      'keyAlias': {
        alias: 'ka',
        description: 'key alias (mastercard developers)'
      },
      'storePass': {
        alias: 'sp',
        description: 'keystore password (mastercard developers)'
      },
      'protoFile': {
        alias: 'pf',
        description: 'protobuf file'
      },
      'verbosity': {
        alias: 'v',
        default: false,
        description: 'log mastercard developers sdk to console'
      }
    });
}

function getProperties(obj) {
  var ret = [];
  for (var name in obj) {
    if (obj.hasOwnProperty(name)) {
      ret.push(name);
    }
  }
  return ret;
}

function guessNested(root) {
  var props = getProperties(root.nested);
  var firstChild = getProperties(root.nested[props[0]].nested);
  return [props[0], firstChild[0]];
}
